//
//  CountriesController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

protocol CountriesDelegate: class {
    func countrySelected(_ country:Country, on controller: CountriesController)
}

class CountriesController: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private enum ListStyle {
        case Group
        case Ungroup
    }
    
    private weak var tableView: UITableView!
    private weak var delegate: CountriesDelegate!
    
    private var style: ListStyle = .Ungroup
    private var countries: [Country] = []
    
    private var filtered: [Country] = []
    private var regions: [String] = []
    private var groupedCountries: [String: [Country]] = [:]
    
    init(with delegate: CountriesDelegate, tableView: UITableView){
        super.init()
        self.delegate = delegate
        self.tableView = tableView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delaysContentTouches = false
    }
    
    public func updateCountries(_ countries: [Country]){
        self.countries = countries
        
        filter()
    }
    
    public func filter(by query: String = ""){
        
        if(query.isEmpty){
            self.filtered = countries
        }else{
            self.filtered = countries.filter{
                $0.name.lowercased().contains(query.lowercased()) || $0.alpha3Code.lowercased().contains(query.lowercased()) }
        }
        
        reloadData()
    }
    
    public func group(){
        style = .Group
        countries.sort{ $0.region < $1.region }
        countries.forEach { (country) in
            if(!groupedCountries.keys.contains(country.region)){
                groupedCountries[country.region] = []
            }
            groupedCountries[country.region]?.append(country)
        }
        
        regions = groupedCountries.keys.sorted{ $0 < $1 }
        
        reloadData()
    }
    
    public func ungroup(){
        style = .Ungroup
        
        groupedCountries.removeAll()
        regions.removeAll()
        
        countries.sort{ $0.name < $1.name }
        
        reloadData()
    }
    
    public func reloadData(){
        self.tableView.reloadData()
    }
    
    private func getCountry(by indexPath: IndexPath) -> Country{
        switch(style){
            case .Group:
                let region = regions[indexPath.section]
                return groupedCountries[region]![indexPath.row]
            case .Ungroup:
                return filtered[indexPath.row]
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch(style){
            case .Group:
                return regions.count
            case .Ungroup:
                return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(style){
            case .Group:
                let region = regions[section]
                return groupedCountries[region]!.count
            case .Ungroup:
                return filtered.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch(style){
            case .Group:
                return regions[section]
            case .Ungroup:
                return ""
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: .Country) as! CountryCell
    
        let country = getCountry(by: indexPath)
        
        cell.flag.text = country.emojiFlag
        cell.title.text = country.name
        cell.subtitle.text = country.codeFormatted
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let country = getCountry(by: indexPath)
        delegate.countrySelected(country, on: self)
    }
    
}
