//
//  DetailController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import UIKit

class DetailController: NSObject {

    private weak var detailView: DetailCountryScreenView!
    
    private lazy var bordersController: BordersController = {
        return BordersController(with: detailView.bordersCollectionView)
    }()
    
    public var country = DetailCountry(){
        didSet{
            showDetail()
        }
    }
    
    init(with view: DetailCountryScreenView){
        super.init()
        self.detailView = view
    }
    
    private func showDetail(){
        detailView.name.text = country.nativeName
        detailView.capital.text = country.capital
        
        detailView.flag.sd_setImage(with: country.flagUrl,
                                    placeholderImage: nil)
        
        detailView.region.text = country.subregion
        detailView.area.text = country.areaFormatted
        detailView.location.text = country.locationFormatted
        
        detailView.population.text = country.populationFormatted
        detailView.language.text = country.languagesFormatted
        detailView.phoneCode.text = country.callingCodesFormatted
        
        detailView.timeZones.text = country.timezonesFormatted
        
        detailView.currency.text = country.currencyFormatted
        
        detailView.map.image = country.map
        
        bordersController.updateBorders(country.countryBorders)
    }
    
}
