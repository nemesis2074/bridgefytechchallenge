//
//  CountriesViewController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit
import Toast_Swift

class CountriesViewController: UIViewController, CountriesDelegate, UISearchBarDelegate {

    @IBOutlet weak var containerView: XibView!
    
    private var detailView: CountriesScreenView{
        return containerView.contentView as! CountriesScreenView
    }
    
    private var groupButton: UIBarButtonItem!
    private var ungroupButton: UIBarButtonItem!
    
    private lazy var listController: CountriesController = {
        return CountriesController(with: self,
                                   tableView: detailView.tableView)
    }()
    
    private func initComponents(){
        title = "title_countries".localized()
        navigationItem.largeTitleDisplayMode = .always
        
        groupButton = UIBarButtonItem(title: "button_group".localized(),
                                      style: .plain,
                                      target: self,
                                      action: #selector(groupCountries))
        
        ungroupButton = UIBarButtonItem(title: "button_ungroup".localized(),
                                        style: .plain,
                                        target: self,
                                        action: #selector(ungroupCountries))
        
        navigationItem.setRightBarButton(groupButton, animated: true)
        
        detailView.searchbar.delegate = self
    }
    
    @IBAction func groupCountries(){
        listController.group()
        detailView.searchbar.text = ""
        detailView.searchbar.isHidden = true
        navigationItem.setRightBarButton(ungroupButton, animated: true)
    }
    
    @IBAction func ungroupCountries(){
        listController.ungroup()
        detailView.searchbar.isHidden = false
        navigationItem.setRightBarButton(groupButton, animated: true)
    }
    
    private func loadCountries(){
        Country.loadCountries { (result) in
            switch(result){
                case .success(let coutries):
                    self.listController.updateCountries(coutries)
                case .failure(let error):
                    self.view.makeToast(error.status.toString())
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        loadCountries()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        listController.filter(by: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func countrySelected(_ country: Country, on controller: CountriesController) {
        let vc = UIStoryboard.instanceDetail()
        vc.countryCode = country.alpha2Code
        navigationController?.pushViewController(vc, animated: true)
    }

}
