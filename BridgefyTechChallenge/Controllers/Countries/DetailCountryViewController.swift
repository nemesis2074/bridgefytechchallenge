//
//  DetailCountryViewController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit
import SDWebImage
import Toast_Swift

class DetailCountryViewController: UIViewController {

    @IBOutlet weak var containerView: XibView!
    
    private var detailView: DetailCountryScreenView{
        return containerView.contentView as! DetailCountryScreenView
    }
    
    private lazy var detailController: DetailController = {
        return DetailController(with: detailView)
    }()
    
    private var saveButton: UIBarButtonItem!
    private var deleteButton: UIBarButtonItem!
    
    public var countryCode: String = ""
    
    private func initComponents(){
        saveButton = UIBarButtonItem(title: "button_save".localized(),
                                     style: .plain,
                                     target: self,
                                     action: #selector(saveCountry))
        
        deleteButton = UIBarButtonItem(title: "button_delete".localized(),
                                       style: .plain,
                                       target: self,
                                       action: #selector(deleteCountry))
        
        navigationItem.setRightBarButton(saveButton, animated: true)
    }
    
    
    
    @IBAction func saveCountry(){
        let alert = createAlert(for: .Save) {
            self.detailController.country.save { (result) in
                switch(result){
                    case .success(_):
                        self.navigationItem.setRightBarButton(self.deleteButton, animated: true)
                    case .failure(let error):
                        self.view.makeToast(error.status.toString())
                }
            }
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func deleteCountry(){
        let alert = createAlert(for: .Delete) {
            self.detailController.country.delete { (result) in
                switch(result){
                    case .success(_):
                        self.navigationItem.setRightBarButton(self.saveButton, animated: true)
                    case .failure(let error):
                        self.view.makeToast(error.status.toString())
                }
            }
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    private func loadDetail(){
        DetailCountry.loadDetail(of: countryCode) { (result) in
            switch(result){
                case .success(let detail):
                    self.navigationItem.title = detail.name
                    self.detailController.country = detail
                    
                    if(detail.isFromPersistence()){
                        self.navigationItem.setRightBarButton(self.deleteButton, animated: true)
                    }else{
                        self.navigationItem.setRightBarButton(self.saveButton, animated: true)
                    }
                    
                case .failure(let error):
                    self.view.makeToast(error.status.toString())
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        loadDetail()
    }
    

}
