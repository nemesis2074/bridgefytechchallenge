//
//  LoginViewController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit
import Toast_Swift

class LoginViewController: UIViewController {

    @IBOutlet weak var containerView: XibView!
    
    private var detailView: LoginScreenView{
        return containerView.contentView as! LoginScreenView
    }
    
    private lazy var keyboardController: KeyboardController = {
        return KeyboardController()
    }()
    
    private func initComponents(){
        
        detailView.loginButton.addTarget(self,
                                         action: #selector(login),
                                         for: .touchUpInside)
    }
    
    @IBAction func login(){
        if(fieldsAreValid()){
            let username = detailView.username.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = detailView.password.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            Authentication.login(username: username, password: password) { (result) in
                switch(result){
                    case .success(_):
                        AppDelegate.shared().showMainSection()
                    case .failure(let error):
                        self.view.makeToast(error.status.toString())
                }
            }
        }
    }
    
    private func fieldsAreValid() -> Bool{
        let username = detailView.username.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = detailView.password.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(username.isEmpty || password.isEmpty){
            self.view.makeToast("alert_msg_empty_fields".localized())
            return false
        }
        
        return true
    }
    
    @IBAction func hideKeyboard(){
        self.view.endEditing(true)
    }
    
    private func configKeyboard(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        detailView.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
        configKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardController.registerObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        keyboardController.unregisterObservers()
    }

}
