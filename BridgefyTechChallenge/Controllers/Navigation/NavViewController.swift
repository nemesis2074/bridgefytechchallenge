//
//  NavViewController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class NavViewController: UINavigationController {

    private func initComponents(){
        self.navigationBar.prefersLargeTitles = true
        
        if #available(iOS 13.0, *) {
            let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: Styles.Color.textPrimary,
                                                             .backgroundColor: Styles.Color.bgGeneral]
            
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = Styles.Color.bgGeneral
            appearance.titleTextAttributes = attributes
            appearance.largeTitleTextAttributes = attributes

            UINavigationBar.appearance().tintColor = Styles.Color.textAccent
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = Styles.Color.textAccent
            UINavigationBar.appearance().barTintColor = Styles.Color.bgGeneral
            UINavigationBar.appearance().isTranslucent = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initComponents()
    }

}
