//
//  TabsViewController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class TabsViewController: UITabBarController {
    
    private func initComponents(){
        
        self.tabBar.barStyle = .default
        self.tabBar.isOpaque = true
        self.tabBar.isTranslucent = false
        self.tabBar.tintColor = Styles.Color.textAccent
        self.tabBar.barTintColor = Styles.Color.bgGeneral
        self.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBar.layer.shadowOffset = CGSize(width: -0.5, height: -0.5)
        self.tabBar.layer.shadowOpacity = Styles.Constants.ShadowOpacity
        self.tabBar.layer.shadowRadius = Styles.Constants.ShadowRadius
        
        self.selectedIndex = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initComponents()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

}
