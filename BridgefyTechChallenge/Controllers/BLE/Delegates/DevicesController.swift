//
//  DevicesController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import UIKit

class DevicesController: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private weak var tableView: UITableView!
    
    private var devices: [BLEDevice] = []
    
    init(with tableView: UITableView){
        super.init()
        self.tableView = tableView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.separatorStyle = .none
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delaysContentTouches = false
    }
    
    public func updateDevices(_ devices: [BLEDevice]){
        self.devices = devices
        reloadData()
    }
    
    public func clear(){
        self.devices.removeAll()
        reloadData()
    }
    
    public func reloadData(){
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: .BLEDevice) as! BLEDeviceCell
    
        let device = devices[indexPath.row]
        
        cell.name.text = device.name
        cell.rssi.text = device.rssi.stringValue
        cell.data.text = device.data
        
        return cell
    }
    
}
