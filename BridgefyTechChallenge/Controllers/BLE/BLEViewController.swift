//
//  BLEViewController.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class BLEViewController: UIViewController, BluetoothManagerDelegate {
    
    @IBOutlet weak var containerView: XibView!
    
    private var detailView: BLEScreenView{
        return containerView.contentView as! BLEScreenView
    }
    
    private var scanButton: UIBarButtonItem!
    
    private lazy var bluetoothManager: BluetoothManager = {
        return BluetoothManager(with: self)
    }()
    
    private lazy var listController: DevicesController = {
        return DevicesController(with: detailView.tableView)
    }()
    
    private func initComponents(){
        title = "title_ble".localized()
        navigationItem.largeTitleDisplayMode = .always
        
        scanButton = UIBarButtonItem(title: "button_scan".localized(),
                                     style: .plain,
                                     target: self,
                                     action: #selector(scanForDevices))
        
        navigationItem.setRightBarButton(scanButton, animated: true)
    }
    
    @IBAction func scanForDevices(){
        listController.clear()
        bluetoothManager.startScaning()
        navigationItem.setRightBarButton(nil, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initComponents()
    }
    
    func didStopScaning() {
        navigationItem.setRightBarButton(scanButton, animated: true)
    }
    
    func devicesUpdated(_ devices: [BLEDevice]) {
        listController.updateDevices(devices)
    }
    
}
