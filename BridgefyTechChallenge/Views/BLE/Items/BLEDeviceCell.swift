//
//  BLEDeviceCell.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import UIKit

class BLEDeviceCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var rssi: UILabel!
    @IBOutlet weak var data: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponents()
    }
    
    private func initComponents(){
        backgroundColor = Styles.Color.bgGeneral
        
        name.textColor = Styles.Color.textPrimary
        name.font = Styles.Font.mediumBold
        
        rssi.textColor = Styles.Color.textSecondary
        rssi.font = Styles.Font.normal
        
        data.textColor = Styles.Color.textSecondary
        data.font = Styles.Font.normal
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
