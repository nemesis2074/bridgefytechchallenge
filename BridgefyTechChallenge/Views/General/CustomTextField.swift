//
//  CustomTextField.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import UIKit

@IBDesignable
class CustomTextField: UITextField {

    @IBInspectable public var hint: String?{
        get{
            return attributedPlaceholder?.string
        }
        set{
            let string = NSAttributedString(string: newValue ?? "",
                                            attributes: [.foregroundColor: Styles.Color.textSecondary ])
            attributedPlaceholder = string
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initComponents()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initComponents()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        initComponents()
    }
    
    private func initComponents() {
        backgroundColor = UIColor.clear
        borderStyle = .none
        textColor = Styles.Color.textPrimary
        font = Styles.Font.normal
        
        
        let underline = CALayer()
        underline.frame = CGRect(x: 0, y: frame.height - 1, width: frame.width, height: 1)
        underline.backgroundColor = Styles.Color.bgSecondary.cgColor
        layer.addSublayer(underline)
    }

}
