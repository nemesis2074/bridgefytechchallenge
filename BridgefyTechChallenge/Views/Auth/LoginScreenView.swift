//
//  LoginScreenView.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class LoginScreenView: UIView {

    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var username: CustomTextField!
    @IBOutlet weak var password: CustomTextField!
    @IBOutlet weak var loginButton: PrimaryButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponents()
    }
    
    private func initComponents(){
        self.backgroundColor = Styles.Color.bgGeneral
        
        username.hint = "hint_username".localized()
        username.keyboardType = .emailAddress
        
        password.hint = "hint_password".localized()
        password.isSecureTextEntry = true
        password.keyboardType = .asciiCapable
        
        loginButton.setTitle("button_login".localized(), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

}
