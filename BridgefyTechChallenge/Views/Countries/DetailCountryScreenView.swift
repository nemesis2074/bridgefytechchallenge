//
//  DetailCountryScreenView.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class DetailCountryScreenView: UIView {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var capital: UILabel!
    
    @IBOutlet weak var regionCardView: UIView!
    @IBOutlet weak var map: UIImageView!
    @IBOutlet weak var region: UILabel!
    @IBOutlet weak var area: UILabel!
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var populationCardView: UIView!
    @IBOutlet weak var population: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var phoneCode: UILabel!
    
    @IBOutlet weak var timeZonesCardView: UIView!
    @IBOutlet weak var timeZones: UILabel!
    
    @IBOutlet weak var currencyCardView: UIView!
    @IBOutlet weak var currency: UILabel!
    
    @IBOutlet weak var bordersCardView: UIView!
    @IBOutlet weak var bordersLabel: UILabel!
    @IBOutlet weak var bordersCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponents()
    }
    
    private func initComponents(){
        self.backgroundColor = Styles.Color.bgGeneral
        
        scrollView.contentInsetAdjustmentBehavior = .never
        
        name.textColor = Styles.Color.textPrimary
        name.font = Styles.Font.largeBold
        
        capital.textColor = Styles.Color.textSecondary
        capital.font = Styles.Font.large
        
        regionCardView.backgroundColor = Styles.Color.bgGeneral
        regionCardView.layer.cornerRadius = Styles.Constants.CornerRadius
        regionCardView.layer.borderColor = Styles.Color.textAccent.cgColor
        regionCardView.layer.borderWidth = 1
        regionCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        regionCardView.layer.shadowOffset = CGSize(width: -1.0, height: -1.0)
        regionCardView.layer.shadowOpacity = Styles.Constants.ShadowOpacity
        regionCardView.layer.shadowRadius = 4
        
        region.textColor = Styles.Color.textSecondary
        region.font = Styles.Font.medium
        
        area.textColor = Styles.Color.textSecondary
        area.font = Styles.Font.medium
        
        location.textColor = Styles.Color.textSecondary
        location.font = Styles.Font.medium
        
        populationCardView.backgroundColor = Styles.Color.bgGeneral
        populationCardView.layer.cornerRadius = Styles.Constants.CornerRadius
        populationCardView.layer.borderColor = Styles.Color.textAccent.cgColor
        populationCardView.layer.borderWidth = 1
        populationCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        populationCardView.layer.shadowOffset = CGSize(width: -1.0, height: -1.0)
        populationCardView.layer.shadowOpacity = Styles.Constants.ShadowOpacity
        populationCardView.layer.shadowRadius = 4
        
        population.textColor = Styles.Color.textSecondary
        population.font = Styles.Font.medium
        
        language.textColor = Styles.Color.textSecondary
        language.font = Styles.Font.medium
        
        phoneCode.textColor = Styles.Color.textSecondary
        phoneCode.font = Styles.Font.medium
        
        timeZonesCardView.backgroundColor = Styles.Color.bgGeneral
        timeZonesCardView.layer.cornerRadius = Styles.Constants.CornerRadius
        timeZonesCardView.layer.borderColor = Styles.Color.textAccent.cgColor
        timeZonesCardView.layer.borderWidth = 1
        timeZonesCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        timeZonesCardView.layer.shadowOffset = CGSize(width: -1.0, height: -1.0)
        timeZonesCardView.layer.shadowOpacity = Styles.Constants.ShadowOpacity
        timeZonesCardView.layer.shadowRadius = 4
        
        timeZones.textColor = Styles.Color.textSecondary
        timeZones.font = Styles.Font.medium
        timeZones.adjustsFontSizeToFitWidth = true
        
        currencyCardView.backgroundColor = Styles.Color.bgGeneral
        currencyCardView.layer.cornerRadius = Styles.Constants.CornerRadius
        currencyCardView.layer.borderColor = Styles.Color.textAccent.cgColor
        currencyCardView.layer.borderWidth = 1
        currencyCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        currencyCardView.layer.shadowOffset = CGSize(width: -1.0, height: -1.0)
        currencyCardView.layer.shadowOpacity = Styles.Constants.ShadowOpacity
        currencyCardView.layer.shadowRadius = 4
        
        currency.textColor = Styles.Color.textSecondary
        currency.font = Styles.Font.medium
        
        bordersCardView.backgroundColor = Styles.Color.bgGeneral
        bordersCardView.layer.cornerRadius = Styles.Constants.CornerRadius
        bordersCardView.layer.borderColor = Styles.Color.textAccent.cgColor
        bordersCardView.layer.borderWidth = 1
        bordersCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
        bordersCardView.layer.shadowOffset = CGSize(width: -1.0, height: -1.0)
        bordersCardView.layer.shadowOpacity = Styles.Constants.ShadowOpacity
        bordersCardView.layer.shadowRadius = 4
        
        bordersLabel.textColor = Styles.Color.textSecondary
        bordersLabel.font = Styles.Font.medium
        
        bordersCollectionView.contentInsetAdjustmentBehavior = .never
        bordersCollectionView.backgroundColor = UIColor.clear
        bordersCollectionView.backgroundView = UIView()
        bordersCollectionView.backgroundView!.backgroundColor = UIColor.clear
        
        bordersCollectionView.register(cell: .Border)
    }

}
