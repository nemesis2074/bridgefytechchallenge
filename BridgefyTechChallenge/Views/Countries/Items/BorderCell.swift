//
//  BorderCell.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class BorderCell: UICollectionViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var flag: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponents()
    }
    
    private func initComponents(){
        backgroundColor = Styles.Color.bgGeneral
        
        cardView.backgroundColor = UIColor.clear
        
        flag.textColor = Styles.Color.textPrimary
        flag.font = Styles.Font.big
        
        name.textColor = Styles.Color.textSecondary
        name.font = Styles.Font.normal
        name.adjustsFontSizeToFitWidth = true
    }

}
