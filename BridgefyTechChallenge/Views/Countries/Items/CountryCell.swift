//
//  CountryCell.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var flag: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initComponents()
    }
    
    private func initComponents(){
        backgroundColor = Styles.Color.bgGeneral
        
        flag.textColor = Styles.Color.textPrimary
        flag.font = Styles.Font.big
        
        title.textColor = Styles.Color.textPrimary
        title.font = Styles.Font.mediumBold
        
        subtitle.textColor = Styles.Color.textSecondary
        subtitle.font = Styles.Font.medium
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
