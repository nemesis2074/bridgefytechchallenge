//
//  PersistenceManager.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import UIKit
import RealmSwift

class PersistenceManager: NSObject {

    public class func configPersistence(){
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        config.fileURL = config.fileURL!
            .deletingLastPathComponent()
            .appendingPathComponent(".default.realm")
        
        Realm.Configuration.defaultConfiguration = config
    }
    
    public class func update(block: () -> Void){
        do{
            let realm = try Realm()
            try realm.write {
                block()
            }
        }catch{ }
    }
    
    public class func clear(){
        do{
            let realm = try Realm()
            try realm.write {
                realm.deleteAll()
            }
        }catch{ }
    }
    
}

//extension RealmSwift.List: Decodable where Element: Decodable {
//    public convenience init(from decoder: Decoder) throws {
//        self.init()
//        let container = try decoder.singleValueContainer()
//        let decodedElements = try container.decode([Element].self)
//        self.append(objectsIn: decodedElements)
//    }
//}
//
//extension RealmSwift.List: Encodable where Element: Encodable {
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encode(self.map { $0 })
//    }
//}
