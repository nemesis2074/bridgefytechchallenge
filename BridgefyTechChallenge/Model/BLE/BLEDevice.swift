//
//  BLEDevice.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import UIKit
import CoreBluetooth

struct BLEDevice {
    let rssi: NSNumber
    let peripheral: CBPeripheral
    let advertisementData: [String: Any]
    
    var id: String{
        return peripheral.identifier.uuidString
    }
    
    var name: String?{
        return peripheral.name
    }
    
    var data: String{
        return advertisementData.description
    }
}
