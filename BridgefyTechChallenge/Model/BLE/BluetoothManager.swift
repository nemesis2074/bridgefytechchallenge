//
//  BluetoothManager.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import Foundation
import CoreBluetooth

protocol BluetoothManagerDelegate: class {
    func didStopScaning()
    func devicesUpdated(_ devices: [BLEDevice])
}

class BluetoothManager: NSObject, CBCentralManagerDelegate {
    
    private var manager: CBCentralManager?
    private var delegate: BluetoothManagerDelegate!
    
    init(with delegate: BluetoothManagerDelegate){
        super.init()
        self.delegate = delegate
    }
    
    private var devices: [UUID: BLEDevice] = [:]{
        didSet{
            let array = devices.map({ $0.value })
            delegate.devicesUpdated(array)
        }
    }
    
    func startScaning(){
        manager = CBCentralManager(delegate: self, queue: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 30) {
            self.stopScaning()
        }
    }
    
    func stopScaning(){
        manager?.stopScan()
        delegate.didStopScaning()
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let device = BLEDevice(rssi: RSSI, peripheral: peripheral, advertisementData: advertisementData)
        devices[peripheral.identifier] = device
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if(central.state == .poweredOn){
            
            if(central.isScanning){
                central.stopScan()
            }
            
            central.scanForPeripherals(withServices: nil)
        }
    }
    
}
