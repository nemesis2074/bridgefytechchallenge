//
//  Endpoints.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation

internal struct Endpoints{
    static var Services = "https://restcountries-v1.p.rapidapi.com"
    static var Images = "https://flagpedia.net/data/flags/w1160/%@.png"
}
