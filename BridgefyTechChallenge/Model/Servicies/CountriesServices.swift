//
//  CountriesServices.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation

internal struct CountriesServices{
    static let All = Endpoints.Services + "/all"
    static let Detail = Endpoints.Services + "/alpha/%@"
}
