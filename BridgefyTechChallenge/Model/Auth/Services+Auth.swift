//
//  Services+Auth.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation

extension ServicesManager{
    
    internal class func login(username: String, password: String,
                              success:@escaping ResponseSuccess<LoginInfo>,
                              fail:@escaping ResponseFail){
        
        let time = Double.random(in: 0.2..<2)
        DispatchQueue.main.asyncAfter(deadline: .now() + time) {
            
            if(username == "challenge@bridgefy.me" &&
                password == "P@$$w0rD!"){
                success(LoginInfo())
            }else{
                let error = ResponseError(status: .UnauthorizedAccess, message: nil)
                fail(error)
            }
        }
        
    }
    
}
