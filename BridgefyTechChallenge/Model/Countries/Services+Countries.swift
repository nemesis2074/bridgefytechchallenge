//
//  Services+Countries.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation
import Alamofire

extension ServicesManager{
    
    internal class func requestCountries(success:@escaping ResponseSuccess<[Country]>,
                                         fail:@escaping ResponseFail){
        
        let headers: HTTPHeaders = [Headers.ApiKey: AppSettings.shared.apiToken,
                                    Headers.ApiHost: AppSettings.shared.apiHost ]
        
        let request = AF.request(CountriesServices.All, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.responseDecodable(of: [Country].self) { (response) in
            switch(response.result){
                case .success(let countries):
                    success(countries)
                case .failure(_):
                    let status = processStatus(response.response)
                    let error = ResponseError(status: status, message: "")
                    fail(error)
            }
        }
    }
    
    internal class func requestDetailCountry(of code: String, success:@escaping ResponseSuccess<DetailCountry>,
                                             fail:@escaping ResponseFail){
        let headers: HTTPHeaders = [Headers.ApiKey: AppSettings.shared.apiToken,
                                    Headers.ApiHost: AppSettings.shared.apiHost ]
        
        let url = String(format: CountriesServices.Detail, code.lowercased())
        
        let request = AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers)
        request.responseDecodable(of: DetailCountry.self) { (response) in
            switch(response.result){
                case .success(let detail):
                    success(detail)
                    
                case .failure(let error):
                    let status = processStatus(response.response)
                    let error = ResponseError(status: status, message: "")
                    fail(error)
            }
        }
    }
    
}
