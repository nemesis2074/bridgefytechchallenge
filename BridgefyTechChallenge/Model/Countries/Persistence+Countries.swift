//
//  Persistence+Countries.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import Foundation
import RealmSwift

extension PersistenceManager{
    
    internal class func saveCountry(_ country: DetailCountry,
                                    success:@escaping ResponseSuccess<Bool>, fail:@escaping ResponseFail){
        do{
            let realm = try Realm()
            try realm.write {
                realm.add(country)
            }
            
            success(true)
            
        }catch{
            let error = ResponseError(status: .DatabaseError, message: nil)
            fail(error)
        }
    }
    
    internal class func loadCountry(by alpha2Code:String,
                                    success:@escaping ResponseSuccess<DetailCountry>, fail:@escaping ResponseFail){
        do{
            let realm = try Realm()
            if let country = realm.object(ofType: DetailCountry.self, forPrimaryKey: alpha2Code){
                success(country)
            }else{
                let resError = ResponseError(status: .NoData, message: nil)
                fail(resError)
            }
            
        }catch{
            let resError = ResponseError(status: .DatabaseError, message: nil)
            fail(resError)
        }
    }
    
    internal class func deleteCountry(_ country: DetailCountry,
                                      success:@escaping ResponseSuccess<Bool>, fail:@escaping ResponseFail){
        do{
            let realm = try Realm()
            try realm.write {
                realm.delete(country)
            }
            
            success(true)
            
        }catch{
            let error = ResponseError(status: .DatabaseError, message: nil)
            fail(error)
        }
    }
    
    internal class func isSaved(_ alpha2Code: String) -> Bool{
        do{
            let realm = try Realm()
            return realm.objects(DetailCountry.self).contains{ $0.alpha2Code == alpha2Code }
        }catch{
            return false
        }
    }
    
}
