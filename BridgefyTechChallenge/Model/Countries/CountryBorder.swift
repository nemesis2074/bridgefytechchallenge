//
//  CountryBorder.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import Foundation

struct CountryBorder {
    let code: String
    
    var emojiFlag: String?{
        return IsoCountryCodes.find(key: code)?.flag
        //return String.emojiFlag(from: alpha2Code)
    }
    
    var name: String?{
        return Locale.current.localizedString(forRegionCode: code.lowercased())
    }
}
