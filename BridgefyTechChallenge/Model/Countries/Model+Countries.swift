//
//  Model+Countries.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation

extension Country{
    
    public static func loadCountries(_ completed:@escaping Completed<[Country]>){
        if(ServicesManager.isInternetAvailable()){
            ServicesManager.requestCountries { (countries) in
                let sorted = countries.sorted{ $0.name.lowercased() < $1.name.lowercased() }
                completed(.success(sorted))
            } fail: { (error) in
                completed(.failure(error))
            }
        }else{
            let error = ResponseError(status: .NoInternet, message: nil)
            completed(.failure(error))
        }
    }
}

extension DetailCountry{
    
    public static func loadDetail(of code: String, completed:@escaping Completed<DetailCountry>){
        if(PersistenceManager.isSaved(code)){
            PersistenceManager.loadCountry(by: code) { (detail) in
                completed(.success(detail))
            } fail: { (error) in
                completed(.failure(error))
            }
        }else{
            if(ServicesManager.isInternetAvailable()){
                ServicesManager.requestDetailCountry(of: code) { (detail) in
                    completed(.success(detail))
                } fail: { (error) in
                    completed(.failure(error))
                }
            }else{
                let error = ResponseError(status: .NoInternet, message: nil)
                completed(.failure(error))
            }
        }
    }
    
    public func isFromPersistence() -> Bool{
        return PersistenceManager.isSaved(alpha2Code)
    }
    
    public func save(_ completed:@escaping Completed<Bool>){
        PersistenceManager.saveCountry(self) { (result) in
            completed(.success(result))
        } fail: { (error) in
            completed(.failure(error))
        }
    }
    
    public func delete(_ completed:@escaping Completed<Bool>){
        PersistenceManager.deleteCountry(self) { (result) in
            completed(.success(result))
        } fail: { (error) in
            completed(.failure(error))
        }
    }
}
