//
//  DetailCountry.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation
import UIKit
import RealmSwift

class DetailCountry: Object, Decodable {
    
    @objc dynamic var alpha2Code: String = ""
    @objc dynamic var alpha3Code: String = ""
    
    @objc dynamic var name: String = ""
    @objc dynamic var nativeName: String  = ""
    @objc dynamic var capital: String  = ""
    
    @objc dynamic var region: String  = ""
    @objc dynamic var subregion: String  = ""
    @objc dynamic var area: Double = 0
    
    public var location = List<DoubleObject>()
    
    @objc dynamic var population: Int = 0
    public var languages = List<String>()
    public var callingCodes = List<String>()
    
    public var timezones = List<String>()
    public var currencies = List<String>()
    public var borders = List<String>()
    
    enum CodingKeys: String, CodingKey {
        case alpha2Code, alpha3Code, name, nativeName,
             capital, region, subregion, area,
             population, languages, callingCodes,
             timezones, currencies, borders
        case location = "latlng"
    }
    
    var flagUrl: URL?{
        let string = String(format: Endpoints.Images, alpha2Code.lowercased())
        return URL(string: string)
    }
    
    var map: UIImage?{
        return UIImage(named: alpha3Code)
    }
    
    var areaFormatted: String? {
        return "\(area.toDecimal()) km²"
    }
    
    var locationFormatted: String{
        if(location.count == 2){
            return "(\(location[0].value), \(location[1].value))"
        }
        return ""
    }
    
    var populationFormatted: String? {
        return population.toDecimal()
    }
    
    var languagesFormatted: String?{
        if(!languages.isEmpty){
            return Locale.current.localizedString(forLanguageCode: languages[0])
        }
        return ""
    }
    
    var callingCodesFormatted: String{
        var format = ""
        callingCodes.forEach { (code) in
            format += "\(code) "
        }
        return format
    }
    
    var timezonesFormatted: String{
        var format = ""
        timezones.forEach { (timezone) in
            let time = timezone.replacingOccurrences(of: "UTC", with: "")
            format += "\(time)\n"
        }
        return format.trimmingCharacters(in: .newlines)
    }
    
    var currencyFormatted: String?{
        if(!currencies.isEmpty){
            return Locale.current.localizedString(forCurrencyCode: currencies[0] )
        }
        return ""
    }
    
    var countryBorders: [CountryBorder]{
        return borders.map{ CountryBorder(code: $0) }
    }
    
    override static func primaryKey() -> String? {
        return "alpha2Code"
    }
}
