//
//  DoubleObject.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import Foundation
import RealmSwift

class DoubleObject: Object, Decodable {
    @objc dynamic var value: Double = 0
    
    override init() {
        super.init()
    }
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.singleValueContainer()
        self.value = try container.decode(Double.self)
    }
}

