//
//  String+Utils.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import Foundation

extension String{
    
    func localized() -> String {
        let localized  = NSLocalizedString(self, comment: "")
        return localized
    }

}
