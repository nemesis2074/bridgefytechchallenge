//
//  UIViewController+Utils.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 09/05/21.
//

import Foundation
import UIKit

public enum AlertType {
    case Save
    case Delete
}

extension UIViewController{
    
    public func createAlert(for type: AlertType,
                            actionBlock:@escaping () -> Void) -> UIAlertController{
        
        var title: String = ""
        var message: String = ""
        var buttonTitle: String = ""
        var buttonStyle: UIAlertAction.Style = .default
        
        switch(type){
            case .Save:
                title = "comfirm_save".localized()
                message = "confirm_save_desc".localized()
                buttonTitle = "button_save".localized()
                buttonStyle = .default
            case .Delete:
                title = "comfirm_delete".localized()
                message = "confirm_delete_desc".localized()
                buttonTitle = "button_delete".localized()
                buttonStyle = .destructive
        }
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        let actionOK = UIAlertAction(title: buttonTitle,
                                     style: buttonStyle) { (action) in
            alert.dismiss(animated: true, completion:nil)
            actionBlock()
        }
        
        let actionCancel = UIAlertAction(title: "button_cancel".localized(),
                                         style: .cancel) { (action) in
            alert.dismiss(animated: true, completion:nil)
        }
        
        alert.addAction(actionOK)
        alert.addAction(actionCancel)
        
        return alert
    }
    
}
