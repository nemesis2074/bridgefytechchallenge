//
//  AppDelegate.swift
//  BridgefyTechChallenge
//
//  Created by Adair de Jesús Castillo Meza on 08/05/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        PersistenceManager.configPersistence()
        
        if(Authentication.isLogin()){
            self.showMainSection()
        }else{
            self.showLoginSection()
        }
        
        return true
    }
}

